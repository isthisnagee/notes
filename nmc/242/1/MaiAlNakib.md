- Background: Author would almost exclusively get questions that asked about the
current political situation in the middle east rather than her work.

- "Likely stems from the general perception...that the Arab world is political and nothing else."

This is a generalization of the Arab world. Problems that stem from this include
    - A "flattening" of the Arab world.
    - Dichotomies between Arab writers and Western Writers

The term "Arab literature" is:
  - A phantom category: a "fake" category with remnant sub-categories. These can be categories that no longer exist.
  - A generalizing category: a category that is "flattening" and creates dichotomies
    - Patronizing:
      - When all literature produced by an arab is assumed to be separate and distinct from other categories/literature
    - Detrimental:
      - When all arabic literature is assumed to be political and nothing else

The author holds a belief that all literary works are inherently political. So contemporary Arabic literature is also political.
It also comes out of a society where the raw material of existence has been reduced to literal rubble.

The study of literature is important because:
  - produces
      - empathy
      - tolerance
  - exposes
      - intolerance
  - emphasizes
      - beauty that exists despite intolerance

IF western readers continue to limit Arab  literature to the political sphere then they will
only see the Arab world as distant--an evening news topic.


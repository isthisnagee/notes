const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

const IMAGE = {
  height: 300,
  width: 300,
  pixel: {
    length: 50
  }
};

const filter = [
  [0, 0, 0],
  [0, 0, 1],
  [0, 0, 0]
  // [1 / 16, 2 / 16, 1 / 16],
  // [2 / 16, 4 / 16, 2 / 16],
  // [1 / 16, 2 / 16, 1 / 16]
];

const INPUTS = new Array(9).fill().map((_, i) => {
  const input = document.createElement("input");
  input.value = filter[i % 3][Math.floor(i / 3)];
  input.onchange = e => {
    filter[i % 3][Math.floor(i / 3)] = e.target.value;
    drawCorrelation();
  };
  return input;
});

const image = [
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 100, 100, 0, 0],
  [0, 0, 100, 100, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0]
];

function step() {}

function drawImage() {
  const length = IMAGE.pixel.length * 6;

  for (let x = 0; x <= IMAGE.width; x += IMAGE.pixel.length) {
    ctx.moveTo(x, 0);
    ctx.lineTo(x, IMAGE.height);
    ctx.stroke();

    ctx.moveTo(0, x);
    ctx.lineTo(IMAGE.width, x);
    ctx.stroke();

    if (x < length)
      for (let y = 0; y < length; y += IMAGE.pixel.length) {
        const idx = n => Math.floor(n / IMAGE.pixel.length);
        const lum = image[idx(y)][idx(x)];
        ctx.fillStyle = `hsl(0,1%, ${lum}%)`;
        ctx.fillRect(x, y, IMAGE.pixel.length, IMAGE.pixel.length);

        ctx.fillStyle = lum > 50 ? "black" : "white";
        ctx.fillText(lum, x + 25, y + 25);
        ctx.stroke();
      }
  }
}

function drawFilterBesidesImage() {
  // filter is 3x3
  const buffer = IMAGE.width + IMAGE.pixel.length;
  const length = IMAGE.pixel.length * 3;
  for (let l = 0; l <= length; l += IMAGE.pixel.length) {
    ctx.moveTo(buffer + l, 0);
    ctx.lineTo(buffer + l, length);
    ctx.stroke();

    ctx.moveTo(buffer, l);
    ctx.lineTo(buffer + length, l);
    ctx.stroke();

    if (l < length)
      for (let y = 0; y < length; y += IMAGE.pixel.length) {
        const idx = n => Math.floor(n / IMAGE.pixel.length);
        const lum = filter[idx(y)][idx(l)];
        const f = n => parseFloat(Math.round(n * 100) / 100).toFixed(2);
        // ctx.fillRect(buffer + l, y, IMAGE.pixel.length, IMAGE.pixel.length);
        // ctx.fillStyle = "black";
        // ctx.fillText(f(lum), buffer + l + 25, y + 25);
        // ctx.stroke();

        const x = idx(l) * 3 + idx(y);
        const input = INPUTS[x];
        input.type = "text";
        input.style.position = "fixed";
        input.style.width = "36px";
        input.style.left = buffer + l + 10 + "px";
        input.style.top = y + 35 + "px";
        document.body.appendChild(input);
      }
  }
}

let I = 0;
let J = 0;
function drawStepCorrelation() {}

function drawCorrelation() {
  const buffer = 2 * IMAGE.width + IMAGE.pixel.length;
  const length = IMAGE.width;
  const mtx = new Array(6).fill().map(_ => new Array(6).fill(0));
  const k = 1;
  for (let i = 0; i < 6; i++) {
    for (let j = 0; j < 6; j++) {
      for (let u = -k; u <= k; u++) {
        for (let v = -k; v <= k; v++) {
          let img, flt;
          try {
            flt = filter[v + k][u + k];
          } catch (e) {
            flt = 0;
          }
          try {
            img = image[j + v][i + u];
          } catch (e) {
            img = 0;
          }
          mtx[j][i] += (flt || 0) * (img || 0);
        }
      }
    }
  }

  for (let l = 0; l <= IMAGE.width; l += IMAGE.pixel.length) {
    ctx.moveTo(buffer + l, 0);
    ctx.lineTo(buffer + l, length);
    ctx.stroke();

    ctx.moveTo(buffer, l);
    ctx.lineTo(buffer + length, l);
    ctx.stroke();

    ctx.moveTo(buffer, l);
    ctx.lineTo(buffer + IMAGE.width, l);
    ctx.stroke();

    if (l < length) {
      for (let y = 0; y < length; y += IMAGE.pixel.length) {
        const idx = n => Math.floor(n / IMAGE.pixel.length);
        const lum = mtx[idx(y)][idx(l)];
        ctx.fillStyle = `hsl(0,1%, ${lum}%)`;
        ctx.fillRect(buffer + l, y, IMAGE.pixel.length, IMAGE.pixel.length);

        ctx.fillStyle = lum > 50 ? "black" : "white";
        const f = n => parseFloat(Math.round(n * 100) / 100).toFixed(2);
        ctx.fillText(f(lum), buffer + l + 25, y + 25);
        ctx.stroke();
      }
    }
  }
}

function getRange(image, i, j, k) {}

function fillBoard() {}

drawImage();
drawFilterBesidesImage();
fillBoard();
drawCorrelation();
function matrixToVector(M) {
  return M.reduce((N, row) => [...N, ...row], []);
}

function Dot(M, N) {
  const MdotN = [];
  for (let i = 0; i < M.length; i++) {
    MdotN.push(M[i] * N[i]);
  }
  return MdotN.reduce((sum, element) => sum + element, 0);
}
